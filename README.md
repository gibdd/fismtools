#Инструменты для ФИС ГИБДД-М ИСОД МВД России
Дополнение для Firefox, обеспечивающее ФИС ГИБДД-М недостающим функционалом.

# Использование материалов
* https://developer.mozilla.org/ru/docs/Mozilla/Add-ons/WebExtensions/Getting_started_with_web-ext

### Установка упаковщика
```shell
npm install --global web-ext
web-ext --version
```

### Упаковка расширения в архив
```shell
web-ext build
```

### Упраковка расширения в XPI
```shell
AMO_JWT_ISSUER=user:10742910:788;
AMO_JWT_SECRET=a558c88d12d5750e7c0a6e196432da7ef012dfdad46f528615f5a5059724a81c;
web-ext sign --api-key=$AMO_JWT_ISSUER --api-secret=$AMO_JWT_SECRET;
```

### Подписание расширения локально
ключи [тут](https://addons.mozilla.org/ru/developers/addon/api/key/)
```bash
web-ext sign --api-key='user:10742910:788' --api-secret='a558c88d12d5750e7c0a6e196432da7ef012dfdad46f528615f5a5059724a81c';
```