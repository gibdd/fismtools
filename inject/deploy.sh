#!/bin/bash

cd ~/Projects/fismtools/inject/ && scp ./injected.* fbd79:/tmp/ && ssh fbd79 'sudo \cp /tmp/injected.* /opt/www/fismtools/'
cd ~/Projects/fismtools/inject/ && scp ./index.html fbd79:/tmp/ && ssh fbd79 'sudo \cp /tmp/index.html /opt/www/fismtools/'

cd ~/Projects/fismtools
rm ./fismtools.zip
zip fismtools.zip ./inject/* ./manifest.json ./package.json ./index.js ./main.js ./icon.png -x ./inject/*.sh
scp ./fismtools.zip fbd79:/tmp/ && ssh fbd79 'sudo \cp /tmp/fismtools.zip /opt/www/fismtools/' && rm ./fismtools.zip
