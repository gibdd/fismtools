console.log('Загружен injected.js v.0.2.4');

/**
 * Данный скрипт представляет из себя костыль, прикрученный колючей проволокой
 * к квадратному колесу и грубо эксплуатирует возможности jQuery.
 * Использование данных формы с моей точки срения должно быть реализовано
 * средствами веб-приложения без затратных операций на селекты jQuery
 * @author В.В. Перегудов <vperegudov@mvd.ru> +7 (495) 214-07-16
 * 
 * В настоящее время скрипт инклудится посредством расширения браузера.
 * В случае добавления в интерфейс ФИС ГИБДД-М функционала, реализованного 
 * данным скриптом прошу создавать в глобальной области объект FismTools
 * со свойством version выше значения переменной FismToolsVersion указанного
 * ниже.
 * О добавлении функционала для исключения из следующих версий скрипта
 * прошу уведомлять меня в кратчайшие сроки.
 */

var FismToolsVersion = 2;

if(typeof FismTools === 'undefined' || FismTools.version < FismToolsVersion) {
var FismTools = (function($){
	var SHOW_PAN = 'Показать панель фильтра';
	var HIDE_PAN = 'Скрыть панель фильтра';

	var menu = $('ul.main-menu');
	var menuItem = $('<li />');


	var FismToolsAlert = function(text, type) {
			//TODO: сделать нормальную уведомлялку
			alert(text);
	};

	var getList = function(menu) {
		for(var i = 0; i < menu.menuItems.length; i++) {
	 		var mi = menu.menuItems[i];
	 		var item = $('<li />', {
	 			html: function() {
	 				var res = '';
	 				if(/^#.+/.test(mi.href) && typeof mi.doClick === 'function') {
	 					var btn = $('<a />', {
	 						href: mi.href,
	 						text: mi.label
	 					});
	 					btn.click(mi.doClick);
	 					return btn;
	 				} else if(typeof mi.href != 'undefined') {
	 					return '<a href="' + mi.href + '" target="_blank">' + mi.label + '</a>';
	 				}
	 			}
	 		});
	 		$(document).on('click', item, function(){
	 			menu.remove();
	 		});
	 		item.appendTo(menu);
	 	}
	};

	var ru2en = function(text) {
		text = text &&
			text
				.toUpperCase()
				.replace(/А/igm, 'A')
				.replace(/В/igm, 'B')
				.replace(/Е/igm, 'E')
				.replace(/К/igm, 'K')
				.replace(/М/igm, 'M')
				.replace(/Н/igm, 'H')
				.replace(/О/igm, 'O')
				.replace(/Р/igm, 'P')
				.replace(/С/igm, 'C')
				.replace(/Т/igm, 'T')
				.replace(/У/igm, 'Y')
				.replace(/Х/igm, 'X');
		return text;
	};

	var oneClickSearch = function(h) {
		$('a > span.k-i-close').trigger('click');
		setTimeout(function(){
			console.log('Инициализация поиска.');
		  $('a#startSearchBtn').trigger('click');
		}, 250);
		window.location.hash = h;
	};

	var getBirthday = function(txt) {
		if(/[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(txt)){
			txt = txt.replace(/([0-9]{4})-([0-9]{2})-([0-9]{2})/, '$3.$2.$1');
			console.log('Дата рождения исправлена на ' + txt);
		} else if(/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/.test(txt)) {
			console.log('Дата рождения корректна');
		} else {
			return '';
		}
		return txt;
	};

	var copyToClipboard = function(text) {
		try { 
			var copytext = document.createElement('input');
    	copytext.value = text;
    	document.body.appendChild(copytext);
    	copytext.select();
    	document.execCommand('copy');
 		} catch(err) { 
    	console.log('Can`t copy, boss'); 
  	}
	};

	/**
	 * Инициализация
	 */

	(function(){
		var wlh = decodeURIComponent(window.location.hash);
		console.log('Чтение хеша - ' + wlh);
		if(/#VIN\:([a-zа-я0-9]{9,17})/i.test(wlh)){
			var vin = wlh.replace(/#VIN\:([a-zа-я0-9]{9,17})/i, '$1');
			console.log('В хеше передан VIN ' + vin);
			$('#filter-vehicle-vin').val(vin);
	 		oneClickSearch('#VIN:' + vin);
		} else if(/#REGNUM\:([a-zа-я0-9]{6,10})/i.test(wlh)) {
			var regnum = wlh.replace(/#REGNUM\:([a-zа-я0-9]{6,10})/i, '$1');
			console.log('В хеше передан госномер ' + regnum);
			$('#filter-vehicle-regNo').val(regnum);
	 		oneClickSearch('#REGNUM:' + regnum);
		} else if(/#PTS\:([a-zа-я0-9]{10})/i.test(wlh)) {
			var pts = wlh.replace(/#PTS\:([a-zа-я0-9]{10})/i, '$1');
			console.log('В хеше передан госномер ' + pts);
			$('#filter-vehicle-ptsNumber').val(pts);
	 		oneClickSearch('#PTS:' + pts);
		}
	})();

	/**
	 * Форма запросов
	 */
	console.log("Форма запросов: переключение отображения Запросов");
	$(document).on('click', '#toggleFilterPanel', function(e) {
		var b = $(e.target);
		var pad = $('.pad');
		pad.toggle();
		
		if(pad.is(':visible'))
			b.text(HIDE_PAN);
		else 
			b.text(SHOW_PAN);

		$('.k-content').resize();

		return false;
	});
	var button = $('<a />', {
		text: HIDE_PAN,
		href: "#",
		id: "toggleFilterPanel"
	});
	menuItem.appendTo(menu);
	button.appendTo(menuItem);


	/**
	 * Карточка нарушения
	 */
	console.log("Карточка нарушения: использование ФИО и ДР для Запросов");
	$(document).on("click", "#personFamily", function(e) {
		var target = $(e.target);
		var menu = $('<ol />', {class: 'fismtools-menu'});
		menu.menuItems = [];
		menu.menuItems.push({
	 			label:  "Использовать ФИО и ДР в Запросах",
	 			href: "#personFamily",
	 			doClick: function(e) {
	 				e.preventDefault();
	 				var personFamily = $("#personFamily").val();
					var personName = $("#personName").val();
					var personFatherName = $("#personFatherName").val();
					var personBirthday = getBirthday($("#personBirthday").val());

	 				if(confirm("Поля формы поиска будут заполнены ФИО и датой рождения. " +
	 					"Желаете очистить другие поля перед поиском?")) {
						document.forms.filterForm.reset();
					}

	 				$("#filter-owner-lastName").val(personFamily);
					$("#filter-owner-firstName").val(personName);
					$("#filter-owner-middleName").val(personFatherName);
					$("#filter-owner-birthDate").val(personBirthday);

					oneClickSearch();
 				}
		});
	  /*	
		menu.menuItems.push({
			label: "Проверка по списку умерших ЗАГС",
			href: "http://zags.gibdd.ru/m/#" + 
				encodeURIComponent($("#personFamily").val()) + "_" +
				encodeURIComponent($("#personName").val()) + "_" +
				encodeURIComponent($("#personFatherName").val()) + "_" +
				getBirthday($("#personBirthday").val()),
			doClick: function(e) {
				alert("Функция в стадии разработки. Если не работает, скоро починю.");
			}
		});

		menu.menuItems.push({
			label: "Искать по ФИО в ФБД Адмпрактика",
			href: "http://fbdap.gibdd.ru/csp/fismadm/web.adm.findap.cls#" + 
				encodeURIComponent($("#personFamily").val()) + "_" +
				encodeURIComponent($("#personName").val()) + "_" +
				encodeURIComponent($("#personFatherName").val()) + "_" +
				getBirthday($("#personBirthday").val()),
			doClick: function(e) {
				alert("Функция в стадии разработки. Если не работает, скоро починю.");
			}
		});
		*/

	 	getList(menu);
	 	target.parent('span').append(menu);
	});

	console.log("Карточка нарушения: использование номера ВУ для Запросов");
	$(document).on("click", "#driveLicenseNumber", function(e) {
		var target = $(e.target);
		var menu = $('<ol />', {class: 'fismtools-menu'});
		menu.menuItems = [];
		
		menu.menuItems.push({
	 			label:  "Использовать номер ВУ Запросах",
	 			href: "#driveLicenseNumber",
	 			doClick: function(e) {
	 				e.preventDefault();
	 				var driveLicenseNumber = $("#driveLicenseNumber").val();

	 				if(confirm("Поля формы поиска будут заполнены номером ВУ " +
						driveLicenseNumber +
	 					". Желаете очистить другие поля перед поиском?")) {
						document.forms.filterForm.reset();
					}

					console.log('Поиск по ВУ ' + driveLicenseNumber);
					$("#filter-owner-driverLicense").val(driveLicenseNumber);

					oneClickSearch();
 				}
		});

	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	console.log("Карточка нарушения: использование УИН для проверки по ГИС ГМП");
	$(document).on("click", "#supplierBillID", function(e) {
		var target = $(e.target);
		var fid = target.val();
	 	var menu = $('<ol />', {class: 'fismtools-menu'});

		menu.menuItems = [];
	 	menu.menuItems[menu.menuItems.length] = {
 			label: 'Поиск в ФБД Адмпрактика',
	 		href: 'http://fbdap.gibdd.ru/csp/fismadm/UI.sys.find1x.cls?UIN=' + encodeURIComponent(fid)
		};
			//href: 'http://fbdap.gibdd.ru/csp/fismadm/web.gisgmpFind.cls?UIN=' + encodeURIComponent(fid)
		if (/^18810[15][0-9]{14}$/.test(fid) && $("#vehicleRegPoint").val() !== ""){
			menu.menuItems[menu.menuItems.length] = {
				label: 'Просмотр фото нарушения',
				href: 'http://fbdap.gibdd.ru/photos/#' + encodeURIComponent(fid) + '|' + encodeURIComponent($("#vehicleRegPoint").val())
			};
		}
	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	console.log("Карточка нарушения: получение постановления суда по УН");
	$(document).on("click", "#unicNumber", function(e) {
		var target = $(e.target);
		var fid = target.val();
	 	var menu = $('<ol />', {class:'fismtools-menu'});
	 	menu.menuItems = [
	 		{
	 			label: 'Поиск в ФБД Адмпрактика',
	 			href: 'http://fbdap.gibdd.ru/csp/fismadm/UI.sys.find1x.cls?ID=' + encodeURIComponent(target.val())
	 		}
	 	];

		var docPath = $('#description').val().replace(/.*(ftp\:\/\/scan264[^\s]+).*/, '$1');
		if (/ftp:\/\/scan264[^\s]+/.test(docPath)) {
	 		menu.menuItems.push({
	 			label: 'Постановление суда',
	 			href: docPath
	 		});
	 	}

	 	getList(menu);
	 	target.parent('td').append(menu);
	});

    console.log("Карточка нарушения: переход на страницу подразделения ГИБДД");
    $(document).on("click", "#execDepartmentCodeShortName", function(e) {
        var target = $(e.target);
        var fid = target.val();
        var menu = $('<ol />', {class:'fismtools-menu'});
        fid = fid.replace(/^(11[0-9]{5})\s.+/, '$1');
        menu.menuItems = [
            {
                label: 'О подразделении на сайте',
                href: 'https://xn--90adear.xn--p1ai/div/' + encodeURIComponent(fid)
            }
        ];

        getList(menu);
        target.parent('td').append(menu);
    });

    $(document).on("click", "#departmentDecidedCodeShortName", function(e) {
        var target = $(e.target);
        var fid = target.val();
        var menu = $('<ol />', {class:'fismtools-menu'});
        fid = fid.replace(/^(11[0-9]{5})\s.+/, '$1');
        menu.menuItems = [
            {
                label: 'О подразделении на сайте',
                href: 'https://xn--90adear.xn--p1ai/div/' + encodeURIComponent(fid)
            }
        ];

        getList(menu);
        target.parent('td').append(menu);
    });

    console.log("Карточка нарушения: поиск по ГРЗ");
	$(document).on("click", "#vehicleRegPoint", function(e) {
		var target = $(e.target);
		var val = target.val();
	 	var menu = $('<ol />', {class: 'fismtools-menu'});
	 	menu.menuItems = [];

	 	if(val == $('#filter-vehicle-regNo').val()) {
	 		FismToolsAlert('Поле ГРЗ уже заполнено в форме запросов');
	 		return;
	 	}
	 	menu.menuItems.push(
	 		{
	 			label:  "Использовать ГРЗ для поиска",
	 			href: "#vehicleRegPoint",
	 			doClick: function() {
	 				e.preventDefault();
	 				console.log(val);
	 				if(confirm("Поле ГРЗ формы запроса заполнено значением: \"" + val
	 						+ "\". Желаете очистить другие поля перед поиском?")) {
	 					document.forms.filterForm.reset();
 					}
	 				$('#filter-vehicle-regNo').val(val);
 					oneClickSearch('#REGNUM:' + val);
 				}
	 		}
	 	);

	 	getList(menu);
	 	target.parent('td').append(menu);
	});

    console.log("Карточка нарушения: возможность загрузки образа постановления (приговора) суда");
	$(document).on("click", "#koapText", function() {
	    if (typeof $('#formPostUpload')[0] != 'undefined' && $('#formPostUpload').is(':visible')) {
            $('#formPostUpload').hide();
	        return false;
        }

        var _this = this;

        function getForm() {
            var form = $('<form />', {
                id: 'formPostUpload',
                enctype:'multipart/form-data',
                action: 'http://upload.gibdd.ru/wd/php/prigovor_upload.php',
                method:'POST',
                target:'_blank',
                class: 'sud-file-form'
            });
            var file = $('<input />', {
                id: 'fileForUploadPost',
                type:'file',
                name: 'upload',
                accept: 'application/pdf',
                class: 'k-button'
            });
            var isp = $('<input />', {
                type: 'hidden',
                name: 'isp',
                value: getSession("fism-session")
            });
            var ispName = $('<input />', {
                type: 'hidden',
                name: 'username',
                value: $('body > div.armd-viewport > div.user-auth-container > span > table > tbody > tr > td:nth-child(2) > span').text()
            });
            var un = $('<input />', {
                type: 'hidden',
                name: 'un',
                value: $('#unicNumber').val()
            });
            var submit = $('<input />', {
                id: 'submitPostUpload',
                type: 'submit',
                class: 'k-button',
                value: 'Загрузить образ',
                click: function() {
                    //form.submit();
                    form.hide();
                    //form.detach();
                    //return false;
                }
            });
            file.appendTo(form);
            isp.appendTo(form);
            ispName.appendTo(form);
            un.appendTo(form);
            submit.appendTo(form);
            return form;
        }

        function getSession(name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1].replace(/([^\s]{8}).+([^\s]{8})/,'$1_$2')) : "0000000000000000";
        }

        var f = getForm();
        $('#koapText').after(f);
        f.show();
        return _this;
    });

	/**
	 * Карточка ТС
	 */
	console.log("Карточка ТС: использование VIN для формы запросов");
	$(document).on("click", "#Vin, #vin", function(e) {
		var target = $(e.target);
		var val = target.val();
	 	var menu = $('<ol />', {class: 'fismtools-menu'});

	 	if(val == $('#filter-vehicle-vin').val()) {
	 		FismToolsAlert('Поле VIN уже заполнено в форме запросов');
	 		return;
	 	}

	 	menu.menuItems = [];
	 	menu.menuItems.push(
	 		{
	 			label:  "Использовать VIN для поиска",
	 			href: "#Vin",
	 			doClick: function(e) {
	 				e.preventDefault();
	 				console.log(val);
	 				if(confirm("Поле VIN формы запроса заполнено значением: \"" + val
	 						+ "\". Желаете очистить другие поля перед поиском?")) {
	 					document.forms.filterForm.reset();
 					}
	 				$('#filter-vehicle-vin').val(val);
	 				oneClickSearch('#VIN:' + val);
 				}
	 		}
	 	);
		menu.menuItems.push(
			{
				label: "Скопировать в буфер обмена",
				href: "#copyToClipboard",
				doClick: function(e) {
					e.preventDefault();
					console.log(val);
					if(confirm("Поле VIN сохраняет похожие на русские буквы в кириллице. Перевести их в английские?")) {
						console.log("Применен транслит.");
						val = ru2en(val);
					}
					copyToClipboard(val);
				}
			}
		);
	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	console.log("Карточка ТС: использование кузова для формы запросов");
	$(document).on("click", "#Kuz, #kuz", function(e) {
		var target = $(e.target);
		var val = target.val();
	 	var menu = $('<ol />', {class: 'fismtools-menu'});

	 	if(val == $('#filter-vehicle-bodyNumber').val()) {
	 		FismToolsAlert('Поле кузова уже заполнено в форме запросов');
	 		return;
	 	}

	 	menu.menuItems = [];
	 	menu.menuItems.push(
	 		{
	 			label:  "Использовать кузов для поиска",
	 			href: "#Vin",
	 			doClick: function(e) {
	 				e.preventDefault();
	 				console.log(val);
	 				if(confirm("Поле кузов формы запроса заполнено значением: \"" + val
	 						+ "\". Желаете очистить другие поля перед поиском?")) {
	 					document.forms.filterForm.reset();
 					}
	 				$('#filter-vehicle-bodyNumber').val(val);
	 				oneClickSearch('#Kuz:' + val);
 				}
	 		}
		);
		menu.menuItems.push(
			{
				label: "Скопировать в буфер обмена",
				href: "#copyToClipboard",
				doClick: function(e) {
					e.preventDefault();
					console.log(val);
					if(confirm("Поле кузов сохраняет похожие на русские буквы в кириллице. Перевести их в английские?")) {
						console.log("Применен транслит.");
						val = ru2en(val);
					}
					copyToClipboard(val);
				}
			}
		);
	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	console.log("Карточка ТС: использование ПТС для формы запросов");
	$(document).on("click", "#PTS, #dpts", function(e) {
		var target = $(e.target);
		var val = target.val();
	 	var menu = $('<ol />', {class: 'fismtools-menu'});

	 	if(val == $('#filter-vehicle-ptsNumber').val()) {
	 		FismToolsAlert('Поле ПТС уже заполнено в форме запросов');
	 		return;
	 	}

	 	menu.menuItems = [];
	 	menu.menuItems.push(
	 		{
	 			label:  "Использовать ПТС для поиска",
	 			href: "#PTS",
	 			doClick: function() {
	 				e.preventDefault();
	 				console.log(val);
	 				if(confirm("Поле ПТС формы запроса заполнено значением: \"" + val
	 						+ "\". Желаете очистить другие поля перед поиском?")) {
	 					document.forms.filterForm.reset();
 					}
	 				$('#filter-vehicle-ptsNumber').val(val);
 					oneClickSearch('#PTS:' + val);
 				}
	 		}
	 	);
	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	console.log("Карточка ТС: использование ГРЗ для формы запросов");
	$(document).on("click", "#Reg_zn", function(e) {
		var target = $(e.target);
		var val = target.val();
	 	var menu = $('<ol />', {class: 'fismtools-menu'});
	 	menu.menuItems = [];

	 	if(val == $('#filter-vehicle-regNo').val()) {
	 		FismToolsAlert('Поле ГРЗ уже заполнено в форме запросов');
	 		return;
	 	}
	 	menu.menuItems.push(
	 		{
	 			label:  "Использовать ГРЗ для поиска",
	 			href: "#Reg_zn",
	 			doClick: function() {
	 				e.preventDefault();
	 				console.log(val);
	 				if(confirm("Поле ГРЗ формы запроса заполнено значением: \"" + val
	 						+ "\". Желаете очистить другие поля перед поиском?")) {
	 					document.forms.filterForm.reset();
 					}
	 				$('#filter-vehicle-regNo').val(val);
 					oneClickSearch('#REGNUM:' + val);
 				}
	 		}
		);
		menu.menuItems.push(
			{
				label: "Скопировать в буфер обмена",
				href: "#copyToClipboard",
				doClick: function(e) {
					e.preventDefault();
					console.log(val);
					copyToClipboard(val);
				}
			}
		);

	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	console.log("Карточка ограничения: использование основания ограничения для поиска электронного документа");
	$(document).on("click", "#osnOgr", function(e) {
		var target = $(e.target);
		var val = target.val();
		val = val.replace(/.*\s([0-9]+[/-][0-9]{4,5})\s.*/, "$1");
	 	var menu = $('<ol />', {class: 'fismtools-menu'});
	 	menu.menuItems = [];

	 	menu.menuItems.push(
			{
                label: 'Найти связанные электронные документы',
                href: 'http://fbdap.gibdd.ru/fssp/#' + val
            }
	 	);

	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	// Исключено из-за неправильного использования
	console.log("Карточка ТС: использование ФИО и ДР заявителя");
	$(document).on("click", "#D_name111", function(e) {
		var target = $(e.target);
		var menu = $('<ol />', {class: 'fismtools-menu'});
		menu.menuItems = [];
		menu.menuItems.push(
	 		{
	 			label:  "Использовать ФИО и ДР для Запросов",
	 			href: "#D_name",
	 			doClick: function() {
	 				e.preventDefault();
	 				var fio = $("#D_name").val().split(' ');

	 				if(typeof fio === 'undefined' || fio.length === 0) {
	 					FismToolsAlert("Не удается разбить ФИО");
	 					return false;
	 				}

	 				var personFamily = fio[0];
					var personName = fio[1];
					var personFatherName = fio[2];
					var personBirthday = getBirthday($("#D_Dateborn").val());

	 				if(confirm("Поля запроса будут заполнены ФИО и датой рождения заявителя. Желаете очистить другие поля перед поиском?")) {
						document.forms.filterForm.reset();
					}


					$("#filter-owner-lastName").val(personFamily);
					$("#filter-owner-firstName").val(personName);
					$("#filter-owner-middleName").val(personFatherName);
					$("#filter-owner-birthDate").val(personBirthday);
					
					oneClickSearch();
 				}
	 		}
	 	);
		// заявитель
		/*
		menu.menuItems.push({
			label: "Проверка ФИОиДР по списку умерших ЗАГС",
			href: function() {
				e.preventDefault();
				var fio = $("#D_name").val().split(' ');
				if(typeof fio === 'undefined' || fio.length === 0) {
					FismToolsAlert("Не удается разбить ФИО");
					return false;
				}
				var personFamily = fio[0];
				var personName = fio[1];
				var personFatherName = fio[2];
				var personBirthday = getBirthday($("#D_Dateborn").val());

				var link = "http://zags.gibdd.ru/m/#" + 
					encodeURIComponent(personFamily) + "_" +
					encodeURIComponent(personName) + "_" +
					encodeURIComponent(personFatherName) + "_" +
					getBirthday(personBirthday);
				return link;
			}(),
			doClick: function(e) {
				alert("Функция в стадии разработки. Если не работает, скоро починю.");
			}
		});
		*/

	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	console.log("Карточка ТС: использование ФИО и ДР собственника");
	$(document).on("click", "#H_name, #H_name", function(e) {
		var target = $(e.target);
		var menu = $('<ol />', {class: 'fismtools-menu'});
		menu.menuItems = [];
		menu.menuItems.push(
	 		{
	 			label:  "Использовать ФИО и ДР для поиска",
	 			href: "#H_name",
	 			doClick: function() {
	 				e.preventDefault();
	 				var fio = $("#H_name").val().split(' ');

	 				if(typeof fio === 'undefined' || fio.length === 0) {
	 					FismToolsAlert("Не удается разбить ФИО");
	 					return false;
	 				}
	 				var personFamily = fio[0];
					var personName = fio[1];
					var personFatherName = fio[2];
					var personBirthday = getBirthday($("#H_Dateborn").val());

	 				if(confirm("Поля запроса будут заполнены ФИО и датой рождения собственника. Желаете очистить другие поля перед поиском?")) {
						document.forms.filterForm.reset();
					}

					$("#filter-owner-lastName").val(personFamily);
					$("#filter-owner-firstName").val(personName);
					$("#filter-owner-middleName").val(personFatherName);
					$("#filter-owner-birthDate").val(personBirthday);
					
					oneClickSearch();
 				}
	 		}
		 );
		
		//собственник
		/*
		menu.menuItems.push({
			label: "Проверка ФИОиДР по списку умерших ЗАГС",
			href: function() {
				e.preventDefault();
				var fio = $("#H_name").val().split(' ');
				if(typeof fio === 'undefined' || fio.length === 0) {
					FismToolsAlert("Не удается разбить ФИО");
					return false;
				}
				var personFamily = fio[0];
				var personName = fio[1];
				var personFatherName = fio[2];
				var personBirthday = getBirthday($("#H_Dateborn").val());

				var link = "http://zags.gibdd.ru/m/#" + 
					encodeURIComponent(personFamily) + "_" +
					encodeURIComponent(personName) + "_" +
					encodeURIComponent(personFatherName) + "_" +
					getBirthday(personBirthday);
				return link;
			}(),
			doClick: function(e) {
				alert("Функция в стадии разработки. Если не работает, скоро починю.");
			}
		});
		*/
	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	console.log("Инструменты для работы со списком нарушений");
	$(document).on("click", "li[aria-controls=application-tab-7] > span.k-link", function(e) {
		var target = $(e.target);
		var menu = $('<ol />', {class: 'fismtools-menu'});
		menu.menuItems = [];
		menu.menuItems.push(
	 		{
	 			label:  "Спрятать архивные нарушения",
	 			href: "#Hide_arhive_viols",
	 			doClick: function() {
	 				e.preventDefault();
	 				$.each($('#searchGridOffences').find('table[role=grid] tr[role=row]'), function(index, e){
	 					if($(e).find('td:first').html() === "T"){
	 						console.log("Текущая " + $(e).find('td:first').html());
	 					}
	 				});
	 			}
	 		}
	 	);

	 	getList(menu);
	 	target.parent('td').append(menu);
	});

	return {
		version: 3
	}
})(jQuery);
}
