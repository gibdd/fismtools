console.log('Подключено расширение Инструменты для ФИС ГИБДД-М v.1.0');
var ts = (new Date().getTime());

var style = document.createElement("link");
style.setAttribute("type", "text/css");
style.setAttribute("rel", "stylesheet");
style.setAttribute("href", "http://info.gibdd.ru/fismtools/injected.css?" + ts);
document.getElementsByTagName("head")[0].appendChild(style);

var script = document.createElement("script");
script.setAttribute("type", "text/javascript");
script.setAttribute("src", "http://info.gibdd.ru/fismtools/injected.js?" + ts);
document.getElementsByTagName("head")[0].appendChild(script);
